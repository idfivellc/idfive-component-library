export class Collapsible {
  // Default opts for collapsible
  protected opts = {
    initialOpen: <string | number>"first", // 0: "none", 1:"first", 2,3,4..., "all"
    useDefaultToggler: true,
    topMenuClass: "menu",
    itemMenuClass: "menu-item",
    subMenuClass: "sub-menu",
    idPrefix: "cgen-",
    recursive: true,
    currentDepth: 0,
    depthLimit: 0, // 0:unlimited, 1 or above: limited accordingly
  };

  protected current: string;
  protected items: Array<HTMLElement>;
  protected accordions: Array<HTMLElement>;

  protected wrapper: HTMLElement;
  protected ul: HTMLElement;

  protected deltaH = 0;

  constructor(wrapper: HTMLElement, opts: object) {
    if (!wrapper) return;

    this.opts = opts && typeof opts == "object" ? { ...this.opts, ...opts } : this.opts;
    this.current = "";

    this.wrapper = wrapper;
    this.ul = this.wrapper.querySelector("." + this.opts.topMenuClass);
    if (!this.ul) this.ul = this.wrapper;

    this.items = Array.from(this.ul.children, (item) => item as HTMLElement);
    this.accordions = [];

    this.init();
  }

  init() {
    this.items.forEach((item, ind) => {
      // get a link
      const btn = item.querySelector("a");
      const subset = <HTMLElement>item.querySelector("." + this.opts.subMenuClass);
      if (subset) {
        // add open/close event handler to the button
        btn.addEventListener("click", this.handleButtonClick.bind(this));
        // add span to show symbol
        if (this.opts.useDefaultToggler) {
          var tagSpan = document.createElement("span");
          tagSpan.classList.add("tag");
          btn.appendChild(tagSpan);
        }

        // revamp item to make it working as Collapsible
        item.addEventListener("sub-height-changed", function(e) {
          const item = this;
          const delta = (<any>e).detail.delta;
          let h = parseFloat(item.dataset.height);
          if (isNaN(h)) h = 0;

          item.dataset.height = String(h + delta);

          const isOpen = item.getAttribute("aria-expanded") == "true";
          if (isOpen) {
            var event = new CustomEvent("apply-height-changed");
            item.dispatchEvent(event);
          }
        });

        item.addEventListener("apply-height-changed", this.applyHeightChanged.bind(this));

        if (!item.classList.contains("menu-item-has-children")) item.classList.add("menu-item-has-children");
        if (!item.getAttribute("data-id") || item.getAttribute("data-id") == "")
          item.setAttribute("data-id", this.opts.idPrefix + ind);

        // Save height value
        item.dataset.height = String(subset.offsetHeight);

        this.toggleAccordion(item, this.opts.initialOpen == "all" ? true : false);
        this.accordions.push(item);

        // Recursive collapsible
        if (this.opts.recursive) {
          const newOpts = { ...this.opts };
          newOpts.currentDepth++;
          if (!(newOpts.depthLimit > 0 && newOpts.currentDepth > newOpts.depthLimit)) {
            newOpts.initialOpen = "none";
            newOpts.idPrefix += ind + "-";
            newOpts.topMenuClass = newOpts.subMenuClass;
            new Collapsible(item, newOpts);
          }
        }
      }
    });

    if (this.accordions.length < 1) return;

    if (this.opts.initialOpen == "first" || this.opts.initialOpen == 1) {
      this.toggleAccordion(this.accordions[0], true);
    } else if (typeof this.opts.initialOpen === "number" && this.opts.initialOpen > 0) {
      if (this.accordions.length >= this.opts.initialOpen) {
        this.toggleAccordion(this.accordions[this.opts.initialOpen - 1], true);
      }
    }
  }

  applyHeightChanged(event) {
    this.setOpenHeight(event.target);
  }

  handleButtonClick(event) {
    event.preventDefault();
    event.stopPropagation();
    const item = event.target.closest("." + this.opts.itemMenuClass);
    this.toggleAccordion(item, undefined);
  }

  toggleAccordion(item: HTMLElement, makeOpen: boolean) {
    if (!item) return;

    let isOpen = typeof makeOpen == "boolean" ? makeOpen : item.getAttribute("aria-expanded") != "true";
    item.setAttribute("aria-expanded", isOpen ? "true" : "false");

    this.onAccordionChanged(item);
  }

  onAccordionChanged(item) {
    const v = item.getAttribute("data-id");
    const isOpen = item.getAttribute("aria-expanded") == "true";
    if (isOpen) {
      // open accordion
      this.current && this.closeAccordion(this.getCurrentAccordion());
      this.openAccordion(item);
      this.current = v;
    } else {
      // close accordion
      this.closeAccordion(item);
      this.current = "";
    }
  }

  openAccordion(item) {
    item.setAttribute("aria-expanded", "true");
    this.setOpenHeight(item);
  }

  setOpenHeight(item) {
    const submenu: HTMLElement = item.querySelector("." + this.opts.subMenuClass);

    const newH = item.dataset.height;
    let prevH = parseFloat(submenu.dataset.prevHeight);
    if (isNaN(prevH)) prevH = submenu.offsetHeight;

    submenu.dataset.prevHeight = newH;
    submenu.style.height = newH + "px";

    let delta = parseFloat(newH);
    if (isNaN(delta)) delta = 0;
    delta = delta - prevH;
    this.changeDeltaH(delta);
  }

  closeAccordion(item) {
    item.setAttribute("aria-expanded", "false");
    const submenu: HTMLElement = item.querySelector("." + this.opts.subMenuClass);
    let prevH = parseFloat(submenu.dataset.prevHeight);
    if (isNaN(prevH)) prevH = submenu.offsetHeight;

    submenu.dataset.prevHeight = "0";
    submenu.style.height = "0px";

    this.changeDeltaH(0 - prevH);
  }

  changeDeltaH(delta) {
    var event = new CustomEvent("sub-height-changed", {
      detail: {
        delta: delta,
      },
    });
    this.wrapper.dispatchEvent(event);
  }

  closeAllAccordions() {
    this.accordions.forEach((accordion) => {
      this.closeAccordion(accordion);
    });
  }

  getCurrentAccordion() {
    const accordion = this.ul.querySelector('[data-id="' + this.current + '"]');
    return accordion;
  }
}

export default function collapsibleInit() {
  Array.from(document.querySelectorAll(".collapsible")).forEach((item) => {
    new Collapsible(item as HTMLElement, null);
  });
}
