class Hamburger {
  protected wrapperClass;
  protected wrapper: HTMLElement;
  protected toggle: HTMLAnchorElement;
  protected body: HTMLElement;
  protected bodyInner: HTMLElement;
  public expanded: boolean = false;

  constructor(wrapper: HTMLElement) {
    if (!wrapper) return;
    this.wrapperClass = "hamburger";
    this.wrapper = wrapper;
    if (!this.wrapper.classList.contains(this.wrapperClass)) this.wrapper.classList.add(this.wrapperClass);
    this.toggle = wrapper.querySelector(".hamburger__anchor");
    this.body = wrapper.querySelector(".hamburger__body");
    this.bodyInner = this.body.querySelector(".hamburger__body__inner");

    this.init();
  }

  protected init() {
    this.toggle.setAttribute("aria-haspopup", "true");
    this.handleToggleClick();
    this.handleOutsideClick();
  }

  protected handleToggleClick() {
    this.toggle.addEventListener("click", (event) => {
      event.preventDefault();
      this.doToggle(!this.expanded);
    });
  }

  protected doToggle(flag: boolean) {
    this.expanded = flag;
    this.wrapper.setAttribute("aria-expanded", this.expanded ? "true" : "false");
    this.body.setAttribute("aria-hidden", flag ? "false" : "true");
  }

  protected handleOutsideClick() {
    const parentLI = this.toggle.parentElement as HTMLElement;
    document.body.addEventListener("click", (event) => {
      const target = event.target as HTMLElement;
      if (!parentLI.contains(target) && this.expanded) {
        this.doToggle(false);
      }
    });
  }

  initAccordionStructure() {
    const ul = this.bodyInner.querySelector("ul.menu");
  }
}

export default function hamburgerInit() {
  Array.from(document.querySelectorAll(".hamburger")).forEach((hamburger: HTMLElement) => {
    if (hamburger.getAttribute("is-custom-handler") != "true") {
      new Hamburger(hamburger);
    }
  });
}
