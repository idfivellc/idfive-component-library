/**
 * Polyfills
 */
import "core-js/stable";
import focusWithin from "focus-within";
import objectFitImages from "object-fit-images";

/**
 * Modules
 */
import { silcCoreInit } from "silc-core";
import { silcAccordionInit } from "silc-accordion";
import { silcNavInit } from "silc-nav";

/**
 * Components
 */
import carouselsInit from "./components/carousel";
import Tablesaw from "./components/table";
import Modal from "./components/modal";
import lazyLoadInit from "./components/_lazyload";
import cardsInit from "./components/card";
import privacyConsentInit from "./components/privacy-consent";
import hamburgerInit from "./components/hamburger";
import collapsibleInit from "./components/collapsible";

/**
 * Utilities
 */
import AccessibilityUtilities from "./utilities/accessibility";

/**
 * Init
 */
focusWithin(document);
objectFitImages();
AccessibilityUtilities.init();
silcCoreInit();
silcAccordionInit();
silcNavInit();
carouselsInit();
Tablesaw.init();
Modal.init();
lazyLoadInit();
cardsInit();
privacyConsentInit();
hamburgerInit();
collapsibleInit();
