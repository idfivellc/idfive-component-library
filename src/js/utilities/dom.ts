export default class DomUtilities {
  private static userTabbing: boolean = false;
  public static get focusableChildSelector(): string {
    return `a[href], button, input, textarea, select, details, [tabindex]:not([tabindex="-1"])`;
  }

  public static getSiblings(e) {
    let siblings = [];
    if (!e.parentNode) {
      return siblings;
    }
    let sibling = e.parentNode.firstChild;

    while (sibling) {
      if (sibling.nodeType === 1 && sibling !== e) {
        siblings.push(sibling);
      }
      sibling = sibling.nextSibling;
    }
    return siblings;
  }
}
