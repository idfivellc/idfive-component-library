# Breadcrumb

## Design Notes
Out of the box, the breadcrumb component supports quick customization of the following properties:

- `margin`, and `padding` of the root breadcrumb element
- `color` of breacrumb links
- `border-color` of the border under each link when hovered over
- Character used as a separator between links