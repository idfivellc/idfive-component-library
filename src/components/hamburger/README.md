# Hamburger

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://bitbucket.org/idfivellc/idfive-component-library/src/master/)

## Summary

Hamburger menu which will open drop down menu when to click.

Clicking outside of dropdown or Hamburger itself will close drop down.

## Behavior options

### Constructor Parameters

It takes 2 parameters:

- `wrapper` : `HTMLElement` type value, which includes whole structure

### Pre-built

By default, we initialize Hamburger for those with `hamburger` class.
If you do not want it work as default, add attribute `is-custom-handler` with value `true`, and then define your own event handler.

ex:

```html
  <div class="hamburger" aria-expanded="false" is-custom-handler="true">
      <a href="#" class="hamburger__anchor">
        <span></span>
        <span></span>
        <span></span>
      </a>
  </div>
```


### Requirement

`wrapper` should contain following elements as mandatory :

```html
    *** Your own HTML code ***
    <a href="#" class="hamburger__anchor">
      <span></span>
      <span></span>
      <span></span>
    </a>
    *** Your own HTML code ***
    <div class="hamburger__body">
      <div class="hamburger__body__inner">
        *** Your own HTML code ***
      </div>
    </div>
    *** Your own HTML code ***
```
