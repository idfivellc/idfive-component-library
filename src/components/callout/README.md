# Callout

## Design Notes
Out of the box, the callout component supports quick customization of the following properties:

- `text-align`, `padding`, `background-size`, and `background-position` of the root callout element
- `font-size`, `font-weight`, and `margin` of the callout's title
- `font-size` and `margin` of the callout's body content