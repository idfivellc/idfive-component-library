# Collapsible (Recursive)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://bitbucket.org/idfivellc/idfive-component-library/src/master/)

## Behavior options

### Constructor Parameters

It takes 2 parameters:

- `wrapper` : `HTMLElement` type value, which includes whole tree


- `opts` (Optional) : `object` type value, which describes how Collapsible will be made



### Pre-built

By default, we initialize Collapsible for those with `collapsible` class.

## Options

### Default Option

```javascript
{
  initialOpen : "first", // 0: "none", 1:"first", 2,3,4..., "all"
  useDefaultToggler: true,
  topMenuClass: "menu",
  itemMenuClass: "menu-item",
  subMenuClass: "sub-menu",
  idPrefix: "cgen-",
  recursive: true,
  currentDepth: 0,
  depthLimit: 0, // 0:unlimited, 1 or above: limited accordingly
}
```

### Possible values & Meaning

#### Meaning

- `topMenuClass` - Top level menu wrapper should have this class assigned.

- `itemMenuClass` - Menu item wrapper's class.

- `subMenuClass` - All sub level menu wrapper should have this class assigned.

- `idPrefix` - Each item is given with auto-generated id, and this prefix will be used that time.

- `recursive` - Whether to apply collapsible recursively or not.

- `depthLimit` - Until how many depth would collapsible be applied. Only used when `recursive` is `true`

#### Possible values

- `initialOpen` : `"first"` or `1`, `"all"`, `"none"` or `0`, or numbers like `2`,`3`,`4`,`5`... meaning `nth` item

- `recursive` : `true`, `false`
