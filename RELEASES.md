# 0.3.10 - 20220923

# 0.3.9 - 20220922

- Updating webpack to properly use and copy local webfonts to build
- Updating webpack to properly use and copy svgs to build
- Updating tsconfig to esModuleInterop - true to use es modules instead of commonjs for imports

# 0.3.8 - 20220906

- Fixing broken webpack config after merge, tag version bump

# 0.3.7 - 20220906

- Updating outdated packages
- Updating to node-sass

# 0.3.6 - 20220803

- Setting importContext to true in fractal.js config

  # 0.3.5 - 20220429

- Fixing incorrect spelling of aria-labelledby in accordion templates
- Fixing links in config.json's that were set to "http://idfive"

  # 0.3.4 - 20220324

- README updates

  # 0.3.3 - 20220314

- Updates prettier config
- Fixes Webpack bug where SVG background images were incorrectly being encoded with the URL Loader
- Adds core-js/stable polyfill
- Fixes CSS transition bug in Modal
- Fixes bug with buddy.yml where incorrect Node.js version was being used in build step

  # 0.3.2 - 20210308

- Updates card component to improve accessibility for keyboard users, support for `data-target` attribute on cards to allow cards to open link in new tab or window.
- Adds .nvmrc

  # 0.3.1 - 20200624

- Adds privacy cookie management component

  # 0.3.0 - 20200214

- Fractal template engine is now using TWIG
- Adds prettier config
- Adds pre-commit hook to run prettier automatically
- Adds object-fit polyfill
- Adds typography base styles
- Adds lazyload component
- Update npm packages
- General bug fixes

  # 0.2.1 - 20190322

- Updates to card module. Removes overlay variant. Fixes padding.
- Updates silc-accordion to 0.10.1
- Adjusts carousel arrows to be visually centered

  # 0.2.0 - 20190314

- Updates silc-accordion to 0.10.0
- Adjusts base variables and adds max-bound, outer-pad
- Hero: Sturctual and styling updates
- Core typography changes
- Adds new modal component using micromodal
- A11y accessibility updates to breadcrumbs
- Carousel updates

  # 0.1.12 - 20190128

- Updates silc-nav to 1.0.6

  # 0.1.11 - 20190128

- Updates silc-nav to 1.0.5

  # 0.1.10 - 20190128

- Updates webpack-dev-server to 3.1.11
- Updates vue to 2.5.22
- Updates silc-nav to 1.0.4

  # 0.1.9 - 20191701

- Replace uglifyjs-webpack-plugin with terser-webpackplugin for ES6 support: https://github.com/mishoo/UglifyJS2/issues/659#issuecomment-447820525

  # 0.1.8 - 20191401

- Adds focus-within polyfill for IE11

  # 0.1.7 - 20181109

- Update webpack config to correctly parse images in CSS
- Small code adjustments

  # 0.1.6 - 20181101

- Vue component styles now get added to index.css build file

  # 0.1.5 - 20181024

- Adds new entry for print styles
- Adds minification for CSS

  # 0.1.4 - 20180924

- Update to silc-nav 1.0.3

  # 0.1.3 - 20180924

- Update to silc-nav 1.0.1

  # 0.1.2 - 20180920

- Pass arguments from npm scrips to library scripts

  # 0.1.1 - 20180914

- Adds default margin/padding for paragraphs and lists
- Updates to card to allow main cta link and background images
- Adds font-weight and text-transform variables to button
- Adds iframe component
- Updates silc-utilities to 0.3.2

  # 0.1.0 - 20180914

- Initial release
